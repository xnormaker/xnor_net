# borrowed from "https://github.com/marvis/pytorch-mobilenet"

import torch.nn as nn
import torch.nn.functional as F

import sys
sys.path.append("../../../Bi-Real-net-master/pytorch_implementation/BiReal18_34")
sys.path.append('C:\\Itsik\\code\\Bi-Real-net-master\\pytorch_implementation\\BiReal18_34')

from birealnet import BinaryActivation, HardBinaryConv
        # out = self.binary_activation(x)
        # out = self.binary_conv(out)
        # out = self.bn1(out)

class MobileNetV1Bin(nn.Module):
    def __init__(self, num_classes=1024):
        super(MobileNetV1Bin, self).__init__()

        def conv_bn(inp, oup, stride):
            return nn.Sequential(
                # BinaryActivation(),
                # HardBinaryConv(inp, oup, 3, stride, 1)
                # nn.BatchNorm2d(oup)
                nn.Conv2d(inp, oup, 3, stride, 1, bias=False),
                nn.BatchNorm2d(oup),
                nn.ReLU(inplace=True)
            )

        def conv_dw(inp, oup, stride):
            return nn.Sequential(
                # BinaryActivation(),
                # HardBinaryConv(inp, inp, 3, stride, 1, groups=inp),
                # nn.BatchNorm2d(inp),
                nn.Conv2d(inp, inp, 3, stride, 1, groups=inp, bias=False),
                nn.BatchNorm2d(inp),
                nn.ReLU(inplace=True),

                BinaryActivation(),
                HardBinaryConv(inp, oup, 1, 1, 0),
                nn.BatchNorm2d(oup)
                # nn.Conv2d(inp, oup, 1, 1, 0, bias=False),
                # nn.BatchNorm2d(oup),
                # nn.ReLU(inplace=True),
            )

        self.model = nn.Sequential(
            conv_bn(3, 32, 2),
            conv_dw(32, 64, 1),
            conv_dw(64, 128, 2),
            conv_dw(128, 128, 1),
            conv_dw(128, 256, 2),
            conv_dw(256, 256, 1),
            conv_dw(256, 512, 2),
            conv_dw(512, 512, 1),
            conv_dw(512, 512, 1),
            conv_dw(512, 512, 1),
            conv_dw(512, 512, 1),
            conv_dw(512, 512, 1),
            conv_dw(512, 1024, 2),
            conv_dw(1024, 1024, 1),
        )
        self.fc = nn.Linear(1024, num_classes)

    def forward(self, x):
        x = self.model(x)
        x = F.avg_pool2d(x, 7)
        x = x.view(-1, 1024)
        x = self.fc(x)
        return x